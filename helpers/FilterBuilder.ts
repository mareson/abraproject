
export class FilterBuilder {
    private result: string = "";

    public conditionStart() {
        this.result += "( ";
        return this;
    }

    public conditionEnd() {
        this.result += " )";
        return this;
    }

    public begins(key: string, value: string, not?: boolean) {
        if (not) this.not();
        this.result += `${key} begins '${value}'`;
        return this;
    }

    public or() {
        this.result += " or ";
        return this;
    }

    public and() {
        this.result += " and ";
        return this;
    }

    public not() {
        this.result += "not ";
        return this;
    }

    public build(format: string): string {
        return `(${this.result}).${format}`;
    }
}