
export function formatZip(zip: string): string {
    return zip.substring(0, 3)+" "+zip.substring(3);
}