import {FC, MouseEventHandler, ReactNode} from "react";
import styled, {css} from "styled-components";
import Link, {LinkProps} from "next/link";
import {UrlObject} from "url";

export interface ButtonProps {
    children: ReactNode;
    onClick?: MouseEventHandler<HTMLButtonElement>;
    className?: string;
    href?: string | UrlObject;
}
const Button = (
    {
        children,
        onClick,
        className,
        href
    }: ButtonProps
) => {
    if (!!href)
        return <WrapperLink href={href} onClick={onClick} className={className}>{children}</WrapperLink>;

    return (
        <WrapperButton onClick={onClick} className={className}>{children}</WrapperButton>
    );
};

export default Button;

const buttonStyle = css`
  border: 1px solid ${({theme})=>theme.colors.primary};
  color: ${({theme})=>theme.colors.primary};
  border-radius: ${({theme})=>theme.borderRadius};
  padding: ${({theme})=>theme.spacing(0.2)} ${({theme})=>theme.spacing(0.8)};
  cursor: pointer;
  appearance: none;
  background-color: ${({theme})=>theme.colors.white};
  font-size: ${({theme})=>theme.typography.body.fontSize};
  
  transition: ${({theme})=>theme.transition.generate(["background-color", "color"])};
  
  ${({theme})=>theme.isHover(css`
    &:hover {
      background-color: ${({theme})=>theme.colors.primary};
      color: ${({theme})=>theme.colors.white};
    }
  `)}
`;

const WrapperButton = styled.button`${buttonStyle}`;
const WrapperLink = styled(Link)`
  text-decoration: none;
  
  ${buttonStyle}
`;