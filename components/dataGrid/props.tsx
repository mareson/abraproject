import {ID} from "../../types/requests";
import {PaginationResponse} from "../../types/responses";
import {MouseEventHandler} from "react";
import {PaginationProps} from "../Pagination";

export type DataGridKey = string;

export interface DataGridRow<I> {
    id: ID;
    item: I;
    [key: DataGridKey]: any;
}

export interface DataGridCol {
    key: DataGridKey;
}

export interface DataGridProps<I, R extends DataGridRow<I>, C extends DataGridCol> {
    rows: R[];
    cols: C[];
    pagination?: PaginationProps;
    loading?: boolean;
}