import {FC} from "react";
import styled, {css} from "styled-components";
import {DataGridCol, DataGridProps, DataGridRow} from "./props";
import Pagination from "../Pagination";
import SkeletonLoading from "../SkeletonLoading";
import {DEFAULT_PAGE_SIZE} from "../../api/processors/query/DataGridQueryProcessor";
import {BreakpointKey} from "../../styled";

const DataGrid = <I, R extends DataGridRow<I>, C extends DataGridCol>(
    {
        rows,
        cols,
        pagination,
        loading
    }: DataGridProps<I, R, C>
) => {
    return (
        <Wrapper>
            <Table>
                { // loading
                    loading && Array.from(Array(DEFAULT_PAGE_SIZE)).map((_, index)=>
                        <Row key={index} $disableSelection>
                            {
                                cols.map((col)=>
                                    <Cell key={col.key}>
                                        Načítání
                                        <LoadingInnerCell>
                                            <SkeletonLoading height="100%" disableBorderRadius />
                                        </LoadingInnerCell>
                                    </Cell>
                                )
                            }
                        </Row>
                    )
                }
                {
                    !loading && rows.map((row)=>
                        <Row key={row.id}>
                            {
                                cols.map((col)=>
                                    <Cell key={col.key}>
                                        {row[col.key]}
                                    </Cell>
                                )
                            }
                        </Row>
                    )
                }
            </Table>
            {
                !!pagination && <StyledPagination {...pagination} />
            }
        </Wrapper>
    );
};

export default DataGrid;

// Border from which the table will be displayed
const TABLE_BREAKPOINT: BreakpointKey = "sm";

const Wrapper = styled.div`

`;

const Table = styled.div`
  display: flex;
  flex-direction: column;
  gap: ${({theme})=>theme.spacing(0.5)};
  ${({theme})=>theme.media.up(TABLE_BREAKPOINT)} {
    display: table;
  }
  
  width: 100%;
  border-collapse: separate;
  border-spacing: ${({theme})=>theme.spacing(0.2)};
`;

const Cell = styled.div`
  display: block;

  &:first-of-type {
    border-top-left-radius: ${({theme})=>theme.borderRadius};
    border-top-right-radius: ${({theme})=>theme.borderRadius};
  }

  &:last-of-type {
    border-bottom-left-radius: ${({theme})=>theme.borderRadius};
    border-bottom-right-radius: ${({theme})=>theme.borderRadius};
  }
  
  ${({theme})=>theme.media.up(TABLE_BREAKPOINT)} {
    display: table-cell;

    &:first-of-type {
      border-top-right-radius: unset;
      border-bottom-left-radius: ${({theme})=>theme.borderRadius};
    }

    &:last-of-type {
      border-top-right-radius: ${({theme})=>theme.borderRadius};
      border-bottom-left-radius: unset;
    }
  }
  
  position: relative;
  overflow: hidden;
  background-color: ${({theme})=>theme.colors.primary};
  color: ${({theme})=>theme.colors.white};
  padding: ${({theme})=>theme.spacing(1)};
  transition: ${({theme})=>theme.transition.generate(["opacity"])};
`;

type RowProps = {
    $disableSelection?: boolean;
};
const Row = styled.div<RowProps>`
  display: block;
  ${({theme})=>theme.media.up(TABLE_BREAKPOINT)} {
    display: table-row;
  }
  
  ${({theme, $disableSelection})=>!$disableSelection && theme.isHover(css`
    & ${Cell} {
      opacity: 0.6;
    }
    
    &:hover {
      & ${Cell} {
        opacity: 1;
      }
    }
  `)}
`;

const LoadingInnerCell = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  display: flex;
  & > * {
    flex-grow: 1;
  }
  align-items: stretch;
  justify-content: stretch;
`;

const StyledPagination = styled(Pagination)`
  margin-top: ${({theme})=>theme.spacing(0.5)};
`;