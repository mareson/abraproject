import {FC, ReactNode} from "react";
import styled from "styled-components";

interface Props {
    children: ReactNode;
}

const PageContainer = (
    {
        children
    }: Props
) => {
    return (
        <OuterWrapper>
            <InnerWrapper>
                {children}
            </InnerWrapper>
        </OuterWrapper>
    );
};

export default PageContainer;

const OuterWrapper = styled.section`
  display: flex;
  justify-content: center;
  padding: 0 ${({theme})=>theme.spacing(3)};
`;

const InnerWrapper = styled.div`
  max-width: ${({theme})=>theme.breakpoints.md}px;
  width: 100%;
`;