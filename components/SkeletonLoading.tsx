import styled, {css, keyframes} from "styled-components";

interface Props {
    height?: string;
    disableBorderRadius?: boolean;
}

const SkeletonLoading = (
    {
        height,
        disableBorderRadius
    }: Props
) => {
    return <Wrapper $height={height} $disableBorderRadius={disableBorderRadius} />;
};

export default SkeletonLoading;

const shimmer = keyframes`
  100% {
    transform: translateX(100%);
  }
`;

type WrapperProps = {
    $height?: string;
    $disableBorderRadius?: boolean;
}
const Wrapper = styled.div<WrapperProps>`
  position: relative;
  overflow: hidden;
  background-color: #DDDBDD;
  width: 100%;
  height: ${({$height})=>$height ?? "50px"};
  flex-grow: 1;

  ${({$disableBorderRadius})=>!$disableBorderRadius && css`
    border-radius: ${({theme})=>theme.borderRadius};
  `}
  
  &:after {
    position: absolute;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    transform: translateX(-100%);
    background-image: linear-gradient(
            90deg,
            rgba(255,255,255, 0) 0,
            rgba(255,255,255, 0.2) 20%,
            rgba(255,255,255, 0.5) 60%,
            rgba(255,255,255, 0)
    );
    animation: ${shimmer} 2s infinite;
    content: '';
  }
`;