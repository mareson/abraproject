import styled, {css} from "styled-components";
import {PaginationResponse} from "../types/responses";
import Button from "./Button";
import {useRouter} from "next/router";

const RANGE: number = 3;

export interface PaginationProps {
    current?: PaginationResponse;
    className?: string;
}

const Pagination = (
    {
        current,
        className
    }: PaginationProps
) => {
    const {query} = useRouter();

    if (!current) return null;

    const page = Number(current.page);
    const pageSize = Number(current.pageSize);
    const total = Number(current.total);
    const lastPage = Math.ceil(Number(total)/Number(pageSize))-1;

    const leftCount: number = (page-RANGE)>=0 ? RANGE : page;
    const rightCount: number = (page+RANGE)<=lastPage ? RANGE : lastPage-page;

    if (lastPage===0) return null;

    return (
        <Wrapper className={className}>
            {
                (page-RANGE>0) &&
                    <Page href={{query: {...query, page: 0}}}>
                        1&nbsp;...
                    </Page>
            }
            {
                leftCount>0 && Array.from(Array(leftCount)).map((value, index)=>
                    <Page key={index} href={{query: {...query, page: (page-leftCount)+index}}}>
                        {(page-leftCount)+index+1}
                    </Page>
                )
            }
            <Page $active>{page+1}</Page>
            {
                rightCount>0 && Array.from(Array(rightCount)).map((value, index)=>
                    <Page key={index} href={{query: {...query, page: page+index+1}}}>
                        {page+index+2}
                    </Page>
                )
            }
            {
                (page+RANGE<lastPage) &&
                    <Page href={{query: {...query, page: lastPage}}}>
                        ...&nbsp;{lastPage+1}
                    </Page>
            }
        </Wrapper>
    );
};

export default Pagination;

const Wrapper = styled.div`
  display: flex;
  gap: ${({theme})=>theme.spacing(0.5)};
  flex-wrap: wrap;
`;

type PageProps = {
    $active?: boolean;
}
const Page = styled(Button)<PageProps>`
  ${({$active})=> $active && css`
    border-color: transparent;
    color: ${({theme})=>theme.colors.black};
    cursor: default;
    user-select: none;
    
    ${({theme})=>theme.isHover(css`
      &:hover {
        background-color: ${({theme})=>theme.colors.white};
        color: ${({theme})=>theme.colors.black};
      }
    `)}
  `}
`;