import {FC, Key, MouseEventHandler} from "react";
import styled, {css} from "styled-components";
import Button, {ButtonProps} from "./Button";

export interface ClickableItemProps extends ButtonProps {
    key: Key,
}

interface Props {
    children: ClickableItemProps[];
    className?: string;
}

const ClickableGroup = (
    {
        children,
        className
    }: Props
) => {
    return (
        <Wrapper className={className}>
            {
                children.map((child)=>
                    <Button {...child} key={child.key}>
                        {child.children}
                    </Button>
                )
            }
        </Wrapper>
    );
};

export default ClickableGroup;

const Wrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: ${({theme})=>theme.spacing(0.2)};
  justify-content: flex-end;
  align-items: center;
`;