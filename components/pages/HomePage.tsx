import PageContainer from "../PageContainer";
import PageHeading from "../PageHeading";
import DataGrid from "../dataGrid/DataGrid";
import useSWR from "swr";
import useRequest from "../../services/RequestService";
import addressBookService from "../../services/AddressBookService";
import {DataGridRow} from "../dataGrid/props";
import {AddressBookItemResponse} from "../../types/responses";
import {useCallback, useEffect, useRef, useState} from "react";
import {formatZip} from "../../helpers/helpers";
import ClickableGroup, {ClickableItemProps} from "../ClickableGroup";
import {useRouter} from "next/router";
import styled, {css} from "styled-components";
import SkeletonLoading from "../SkeletonLoading";
import DataGridQueryProcessor from "../../api/processors/query/DataGridQueryProcessor";
import {ZipGroupingQueryProcessor} from "../../api/processors/query/ZipGroupingQueryProcessor";

interface Row extends DataGridRow<AddressBookItemResponse> {
    code: string;
    name: string;
    zip: string;
}

interface Props {
}

const HomePage = (
    {}: Props
) => {
    const {run} = useRequest(addressBookService.retrieve);
    const {query} = useRouter();
    const dataGridProcessor = useRef<DataGridQueryProcessor>(new DataGridQueryProcessor(query));
    const zipGroupingProcessor = useRef<ZipGroupingQueryProcessor>(new ZipGroupingQueryProcessor(query));

    const [rows, setRows] = useState<Row[]>([]);
    const [page, setPage] = useState<string>(dataGridProcessor.current.getPage()+"");
    const [zipStartsWith, setZipStartsWith] = useState<string>(zipGroupingProcessor.current.getZipStartsWith());
    const [omit, setOmit] = useState<string[]>(zipGroupingProcessor.current.getOmit());

    useEffect(()=>{
        dataGridProcessor.current = new DataGridQueryProcessor(query);
        zipGroupingProcessor.current = new ZipGroupingQueryProcessor(query);
        setPage(dataGridProcessor.current.getPage()+"");
        setZipStartsWith(zipGroupingProcessor.current.getZipStartsWith());
        setOmit(zipGroupingProcessor.current.getOmit());
    }, [query]);

    let {data} = useSWR({page, zipStartsWith, omit}, (p)=>run(p), {
        revalidateIfStale: false,
        revalidateOnFocus: false,
        revalidateOnReconnect: false
    });

    useEffect(()=>{
        if (!data) return;
        setRows(data.data.map((item)=>({
            ...item,
            item
        })));
    }, [data]);

    const isAppliedFilter = useCallback((): boolean=>{
        if (!data) return false;
        return (data.filter.zip.commonStartsWith!=="" || data.filter.zip.omit.length>0);
    }, [data]);

    return (
        <PageContainer>
            <PageHeading>ABRA projekt</PageHeading>
            {
                ((data && data.filter.zip.groups.length > 0) || isAppliedFilter()) &&
                    <>
                        <StyledClickableGroup>{
                            (() => {
                                const result: ClickableItemProps[] = data?.filter.zip.groups.map((group)=>({
                                    key: group.startsWith,
                                    href: {query: {page: 0, zipStartsWith: group.startsWith}},
                                    children: formatZip(group.startsWith.padEnd(5, "x"))
                                })) ?? [];

                                if (data?.filter.zip.isOther)
                                    result.push({
                                        key: "other",
                                        children: "Ostatní",
                                        href: {query: {
                                            page: 0,
                                            zipStartsWith,
                                            omit: data.filter.zip.groups.map((group)=>group.startsWith)
                                                .concat(data.filter.zip.omit)
                                        }}
                                    });

                                if (isAppliedFilter())
                                    result.push({
                                        key: "clear",
                                        children: "",
                                        className: "clear",
                                        href: {query: {page: 0}}
                                    });

                                return result;
                            })()
                        }</StyledClickableGroup>
                        <Divider />
                    </>
            }
            {
                !data &&
                    <>
                        <SkeletonLoading height="20px" />
                        <Divider />
                    </>
            }
            <DataGrid
                cols={[
                    {key: "code"},
                    {key: "name"},
                    {key: "zip"}
                ]}

                rows={rows}
                loading={!data}

                pagination={{
                    current: data?.pagination
                }}
            />
        </PageContainer>
    );
};

export default HomePage;

// pixel
const CLEAR_LINE_WIDTH: number = 2;

const StyledClickableGroup = styled(ClickableGroup)`
  & .clear {
    border-color: ${({theme})=>theme.colors.error};
    color: ${({theme})=>theme.colors.error};
    position: relative;
    width: 25px;
    height: 25px;
    
    &::before, &::after {
      position: absolute;
      content: "";
      top: calc(50% - ${CLEAR_LINE_WIDTH/2}px);
      left: 0;
      width: 100%;
      height: ${CLEAR_LINE_WIDTH}px;
      background-color: ${({theme})=>theme.colors.error};
      transform: rotate(45deg);
      border-radius: ${CLEAR_LINE_WIDTH}px;
      transition: ${({theme})=>theme.transition.generate(["background-color"])};
    }

    &::after {
      transform: rotate(-45deg);
    }
    
    ${({theme})=>theme.isHover(css`
      &:hover {
        background-color: ${({theme})=>theme.colors.error};

        &::before, &::after {
          background-color: ${({theme})=>theme.colors.white};
        }
      }
    `)}
  }
`;

const Divider = styled.div`
  height: ${({theme})=>theme.spacing(0.5)};
`;