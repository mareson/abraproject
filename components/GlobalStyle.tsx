import {createGlobalStyle, css, DefaultTheme} from "styled-components";

const GlobalStyle = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    box-sizing: border-box;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
    Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    ${({theme})=>theme.typography.generate("body")}
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  h1 {
    ${({theme})=>theme.typography.generate("h1")}
    margin: 0;
  }
`;

export default GlobalStyle;