import {FC, ReactNode} from "react";
import styled from "styled-components";

interface Props {
    children: ReactNode;
}

const PageHeading = (
    {
        children
    }: Props
) => {
    return <Wrapper>{children}</Wrapper>;
};

export default PageHeading;

const Wrapper = styled.h1`
  margin-top: ${({theme})=>theme.spacing(3)};
  margin-bottom: ${({theme})=>theme.spacing(2)};
`;