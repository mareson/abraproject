import {useCallback, useMemo} from "react";
import {toast} from "react-toastify";

export type MessageHandler = (message: string)=>void;

export type MessagesHandler = {
    success: MessageHandler;
    error: MessageHandler;
    info: MessageHandler;
};

export function useMessages(): MessagesHandler {

    const success = useCallback(
        (message: string)=>toast.success(message),
        []
    );

    const error = useCallback(
        (message: string)=>
            toast.error(message),
        []
    );

    const info = useCallback(
        (message: string)=>
            toast.info(message),
        []
    );

    return useMemo(()=>({success, error, info}), [success, error, info]);
}