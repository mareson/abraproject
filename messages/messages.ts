export enum Message {
    SOMETHING_WENT_WRONG,

    // API MESSAGES
    UNSUPPORTED_METHOD,
    ABRA_REQUEST_FAILED
}

export const MessageContent_CS: {
    [key in Message]: string
} = {
    [Message.SOMETHING_WENT_WRONG]: "Něco se nepovedlo",

    // API MESSAGES
    [Message.UNSUPPORTED_METHOD]: "Nepodporovaná metoda",
    [Message.ABRA_REQUEST_FAILED]: "Nepodařilo se získat data z Abry"
}
