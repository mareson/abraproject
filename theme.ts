import {css, DefaultTheme} from "styled-components";
import {BreakpointKey} from "./styled";

export const BASIC_SPACING: number = 10; // px

export const BASIC_TRANSITION_DURATION: number = 300; // ms

export const BASIC_BORDER_RADIUS: number = 5; // px

export const COLORS = {
    primary: "rgb(255,171,38)",
    secondary: "rgb(148,94,20)",
    grey: {
        main: "rgb(230,230,230)",
        darker: "rgb(192,192,192)",
        dark: "rgb(160,160,160)",
        generate: (strength: number)=>`rgb(${strength},${strength},${strength})`
    },
    white: "rgb(255,255,255)",
    black: "rgb(0,0,0)",
    info: "rgb(98,156,68)",
    error: "rgb(208,37,37)"
};

const BREAKPOINT: {
    [key in BreakpointKey]: number
} = {
    xs: 0,
    sm: 600,
    md: 900,
    lg: 1200,
    xl: 1536
} // px

export const THEME: DefaultTheme = {
    colors: COLORS,
    basicSpacing: BASIC_SPACING,
    spacing: (factor)=>`${factor*BASIC_SPACING}px`,
    borderRadius: `${BASIC_BORDER_RADIUS}px`,
    pageSideMargins: 3,
    pageMaxWidth: BREAKPOINT.lg,
    breakpoints: {
        xs: BREAKPOINT.xs,
        sm: BREAKPOINT.sm,
        md: BREAKPOINT.md,
        lg: BREAKPOINT.lg,
        xl: BREAKPOINT.xl
    },
    media: {
        up: (key: BreakpointKey)=>`@media all and (min-width: ${BREAKPOINT[key]}px)`,
        down: (key: BreakpointKey)=>`@media all and (max-width: ${BREAKPOINT[key]}px)`,
    },
    transition: {
        duration: `${BASIC_TRANSITION_DURATION}ms`,
        generate: (properties)=>properties.map(
            (property)=>`${property} ${BASIC_TRANSITION_DURATION}ms ease`
        ).join(", ")
    },
    typography: {
        body: { fontSize: "1em", color: COLORS.black, fontWeight: "normal" },
        h1: { fontSize: "1.8em", color: COLORS.primary, fontWeight: "normal" },
        h2: { fontSize: "1.4em", color: COLORS.secondary, fontWeight: "normal" },
        description: {fontSize: "12px", color: COLORS.grey.darker, fontWeight: "normal"}, // because of safari, we have to use pixels
        generate: (key)=>css`
          font-size: ${({theme})=>theme.typography[key].fontSize};
          color: ${({theme})=>theme.typography[key].color};
          font-weight: ${({theme})=>theme.typography[key].fontWeight};
        `
    },
    isHover: (children)=>css`
      @media(hover: hover) {
        ${children}
      }
    `
};