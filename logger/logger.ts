import pino from "pino";

const logger = pino(
    {},
    pino.destination(`${__dirname}/pino-logger.log`)
);

export default logger;