import {Message, MessageContent_CS} from "../messages/messages";
import {NextApiResponse} from "next";
import {ErrorResponse} from "../types/responses";

export enum ApiError {
    UNSUPPORTED_METHOD,
    ABRA_REQUEST_FAILED
}

interface ErrorType {
    messageKey: Message,
    status: number
}

export const Errors: {
    [key in ApiError]: ErrorType
} = {
    [ApiError.UNSUPPORTED_METHOD]: {
        messageKey: Message.UNSUPPORTED_METHOD,
        status: 405
    },
    [ApiError.ABRA_REQUEST_FAILED]: {
        messageKey: Message.ABRA_REQUEST_FAILED,
        status: 500
    }
};

export function handleError(
    res: NextApiResponse,
    apiErrorKey: ApiError,
    params?: Partial<ErrorResponse>
) {
    const error = Errors[apiErrorKey];
    const errorResponse: ErrorResponse = {
        status: error.status,
        message: MessageContent_CS[error.messageKey]
    };
    res.status(error.status).json({...errorResponse, ...params});
}