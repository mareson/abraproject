import {PaginationRequest} from "../../../types/requests";
import {ParsedUrlQuery} from "querystring";
import {PaginationResponse} from "../../../types/responses";
import BaseQueryProcessor, {processNumericQuery} from "./BaseQueryProcessor";

export const DEFAULT_PAGE_SIZE: number = 20;

export default class DataGridQueryProcessor extends BaseQueryProcessor {

    private pagination: PaginationRequest;
    private storedPage: number | undefined = undefined;
    private storedPageSize: number | undefined = undefined;

    constructor(query: ParsedUrlQuery) {
        super(query);
        this.pagination = query;
    }

    public getPage(): number {
        if (this.storedPage !== undefined) return this.storedPage;

        this.storedPage = processNumericQuery(this.pagination.page);
        return this.storedPage;
    }

    public getPageSize(): number {
        if (this.storedPageSize !== undefined) return this.storedPageSize;

        this.storedPageSize = processNumericQuery(this.pagination.pageSize, DEFAULT_PAGE_SIZE);
        return this.storedPageSize;
    }

    public getOffset(): number {
        return this.getPageSize() * this.getPage();
    }

    public getPagination(total: string): PaginationResponse {
        return {
            page: this.getPage() + "",
            pageSize: this.getPageSize() + "",
            total
        };
    }
}