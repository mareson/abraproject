import {AddressBookRequest} from "../../../types/requests";
import {ParsedUrlQuery} from "querystring";
import BaseQueryProcessor, {processArrayQuery, processNumericQuery} from "./BaseQueryProcessor";

export class ZipGroupingQueryProcessor extends BaseQueryProcessor {
    private addressBookRequest: AddressBookRequest;
    private storedOmit: string[] | undefined = undefined;

    constructor(query: ParsedUrlQuery) {
        super(query);
        this.addressBookRequest = query;
    }

    public getOmit(): string[] {
        if (this.storedOmit !== undefined) return this.storedOmit;

        this.storedOmit = processArrayQuery(this.addressBookRequest.omit);
        return this.storedOmit;
    }

    public isOmit(zipStartsWith: string): boolean {
        const omit = this.getOmit();

        for (let omitStartsWith of omit)
            if (zipStartsWith.indexOf(omitStartsWith)===0) return true;

        return false;
    }

    public getZipStartsWith(): string {
        return this.addressBookRequest.zipStartsWith ?? "";
    }
}