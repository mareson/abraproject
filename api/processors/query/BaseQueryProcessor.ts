import {ParsedUrlQuery} from "querystring";
import BaseProcessor from "../BaseProcessor";


export default class BaseQueryProcessor extends BaseProcessor {
    protected query: ParsedUrlQuery;

    constructor(query: ParsedUrlQuery) {
        super();
        this.query = query;
    }
}

export function processNumericQuery(query: string | undefined, defaultValue: number = 0): number {
    if (!query) return defaultValue;
    const numericQuery: number = Number(query);
    if (isNaN(numericQuery)) return 0;
    return numericQuery;
}

export function processArrayQuery(query: string | string[] | undefined, defaultValue: string[] = []): string[] {
    if (!query) return defaultValue;
    if (!Array.isArray(query)) {
        if (query==="") return defaultValue;
        return [query];
    }
    return query;
}