import BaseProcessor from "./BaseProcessor";
import {AddressBookGroupResponse, AddressBookGroupsResponse} from "../../types/responses";
import abraService from "../../services/external/AbraService";
import {ParsedUrlQuery} from "querystring";
import {ZipGroupingQueryProcessor} from "./query/ZipGroupingQueryProcessor";

const GROUP_MAX_SIZE: number = 5;
const DEPTH: number = 2;
const DECOMPOSITION_GROUP_SIZE: number = 3;

type MaxCount = {count: number, index: number};

export class ZipGroupingProcessor extends BaseProcessor {
    private queryProcessor: ZipGroupingQueryProcessor;

    constructor(query: ParsedUrlQuery) {
        super();
        this.queryProcessor = new ZipGroupingQueryProcessor(query);
    }

    private getGroup(startsWith: string): Promise<AddressBookGroupResponse> {
        return new Promise(async(resolve, reject)=>{
            if (this.queryProcessor.isOmit(startsWith)) {
                reject();
                return;
            }

            try {
                const response = await abraService.retrieveAddressBook({
                    limit: 1,
                    start: 0,
                    zipStartsWith: startsWith
                });

                resolve({
                    count: Number(response.winstrom["@rowCount"]),
                    startsWith
                });
            } catch (e) {
                reject();
            }
        });
    }

    private async getPotentialGroups(startsWith: string): Promise<AddressBookGroupResponse[]> {
        const promises: Promise<AddressBookGroupResponse>[] = [];

        for (let i=0; i<10; i++)
            promises.push(this.getGroup(startsWith+i));
        const result = await Promise.allSettled(promises);

        const successfulGroups: AddressBookGroupResponse[] = [];
        let max: MaxCount = {count: 0, index: 0};
        result.forEach((subResult)=>{
            if (subResult.status==="fulfilled") {
                if (subResult.value.count>max.count)
                    max = {count: subResult.value.count, index: successfulGroups.length};
                successfulGroups.push(subResult.value);
            }
        });

        return successfulGroups;
    }

    public async getGroups(): Promise<AddressBookGroupsResponse> {
        const startsWith: string = this.queryProcessor.getZipStartsWith();
        const responseObject: AddressBookGroupsResponse = {
            groups: [],
            isOther: false,
            commonStartsWith: startsWith,
            omit: this.queryProcessor.getOmit()
        };
        if (startsWith.length===5) return responseObject;

        let result: AddressBookGroupResponse[] = await this.getPotentialGroups(startsWith);

        for (let i=0; i<DEPTH; i++) {
            result = result.filter((group)=>group.count!==0);
            result.sort((a, b)=>b.count-a.count);
            if (i===DEPTH-1 || result.length<=1) break;
            const promises: Promise<AddressBookGroupResponse[]>[] = [];
            for (let j=0; j<DECOMPOSITION_GROUP_SIZE && j<result.length; j++) {
                promises.push(this.getPotentialGroups(result[j].startsWith));
            }
            result.splice(0, DECOMPOSITION_GROUP_SIZE);
            const subGroupsResults = await Promise.allSettled(promises);
            subGroupsResults.forEach((subGroupResult)=>{
                if (subGroupResult.status==="fulfilled")
                    result = result.concat(subGroupResult.value);
            });
        }

        if (result.length>=GROUP_MAX_SIZE) {
            result.splice(GROUP_MAX_SIZE-1);
            responseObject.isOther = true;
        }

        responseObject.groups = result;
        return responseObject;
    }
}