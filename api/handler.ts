import {NextApiRequest, NextApiResponse} from "next";
import {ApiError, handleError} from "./errors";

export type ApiHandler = (request: NextApiRequest, response: NextApiResponse)=>void;

export type EndpointMethods = {
    [method: string]: ApiHandler;
};

export default async function basicHandler(
    request: NextApiRequest,
    response: NextApiResponse,
    methods: EndpointMethods
) {
    const method: string | undefined = request.method?.toUpperCase();
    if (!!method && method in methods) {
        return methods[method](request, response);
    }

    handleError(response, ApiError.UNSUPPORTED_METHOD);
}