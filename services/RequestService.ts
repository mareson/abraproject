import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {useCallback, useMemo, useState} from "react";
import {ErrorResponse} from "../types/responses";
import {Message, MessageContent_CS} from "../messages/messages";
import {useMessages} from "../messages/messagesHandler";

axios.defaults.baseURL = (process.env.SERVER_APP_URL ?? "") + "/api";

export enum HttpMethod {
    POST = "POST",
    PUT = "PUT",
    GET = "GET",
    DELETE = "DELETE",
    PATCH = "PATCH"
}

export class RequestService {
    protected apiAddress: string = "";

    constructor(apiAddress: string) {
        this.apiAddress = apiAddress;
    }

    protected request = (url: string, method: HttpMethod, data?: any, controller?: AbortController, config?: AxiosRequestConfig) =>
        axios.request({
            url,
            method,
            ...config,
            signal: controller?.signal,
            data
        });
}

export default function useRequest<T, P>(
    process: (params: P)=>Promise<AxiosResponse<T>>
): {
    run: (params: P)=>Promise<null | T>;
    loading: boolean;
    startLoading: ()=>void;
    stopLoading: ()=>void;
} {
    const messages = useMessages();
    const [loading, setLoading] = useState<boolean>(false);

    const startLoading = useCallback(() => setLoading(true), []);
    const stopLoading = useCallback(() => setLoading(false), []);

    const run = useCallback((params: P): Promise<null | T> => {
        startLoading();

        return new Promise<null | T>((resolve)=>{
            process(params)
                .then((result)=>{
                    resolve(result.data);
                })
                .catch((e: AxiosError<ErrorResponse>)=>{
                    console.error(e);

                    messages.error(e.response?.data.message ?? MessageContent_CS[Message.SOMETHING_WENT_WRONG]);
                })
                .finally(()=>stopLoading())
        });
    }, [stopLoading, startLoading, process]);

    return useMemo(()=>({
        startLoading, stopLoading, loading, run
    }), [startLoading, stopLoading, loading, run]);
}