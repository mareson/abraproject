import {HttpMethod, RequestService} from "../RequestService";
import {AbraAddressBookResponse} from "../../types/responses";
import {AxiosResponse} from "axios";
import logger from "../../logger/logger";
import {ApiError} from "../../api/errors";
import {FilterBuilder} from "../../helpers/FilterBuilder";
import {formatZip} from "../../helpers/helpers";

export interface RetrieveAddressBookOpt {
    limit: number;
    start: number;
    zipStartsWith?: string;
    omit?: string[];
}

const ZIP_KEY: string = "psc";

function zipBegins(builder: FilterBuilder, zipStartsWith: string, not?: boolean) {
    builder.begins(ZIP_KEY, zipStartsWith, not);
    if (zipStartsWith.length>3) builder.or().begins(ZIP_KEY, formatZip(zipStartsWith), not);
}

export class AbraService extends RequestService {
    private abraRequestFailed(e: unknown, reject: (reason?: any) => void) {
        logger.error(e);
        reject(ApiError.ABRA_REQUEST_FAILED);
    }

    public retrieveAddressBook = async ({limit, start, zipStartsWith, omit}: RetrieveAddressBookOpt): Promise<AbraAddressBookResponse> =>
        new Promise(async (resolve, reject)=>{
            try {
                const filterBuilder = new FilterBuilder();
                if (!!zipStartsWith && zipStartsWith!=="") {
                    filterBuilder.conditionStart();
                    zipBegins(filterBuilder, zipStartsWith);
                    filterBuilder.conditionEnd();
                }

                if (!!omit && omit.length>0) {
                    if (!!zipStartsWith && zipStartsWith!=="")
                        filterBuilder.and();
                    filterBuilder.conditionStart();
                    for (let i = 0; i < omit.length; i++) {
                        zipBegins(filterBuilder, omit[i], true);
                        if (i !== omit.length - 1)
                            filterBuilder.and();
                    }
                    filterBuilder.conditionEnd();
                }

                const search = new URLSearchParams();
                search.set("detail", "custom:psc,kod,nazev");
                search.set("limit", limit+"");
                search.set("start", start+"");
                search.set("add-row-count", "true");

                const result: AxiosResponse<AbraAddressBookResponse> =
                    await this.request(
                        this.apiAddress + `/adresar/${filterBuilder.build("json")}?${search.toString()}`,
                        HttpMethod.GET
                    );
                resolve(result.data);
            } catch (e) {
                this.abraRequestFailed(e, reject);
            }
        });
}

const abraService = new AbraService("https://demo.flexibee.eu/v2/c/demo");
export default abraService;