import {HttpMethod, RequestService} from "./RequestService";
import {AddressBookResponse} from "../types/responses";
import {AxiosResponse} from "axios";

interface RetrieveOpt {
    page: string;
    zipStartsWith: string;
    omit: string[];
}

export class AddressBookService extends RequestService {
    public retrieve = ({page, zipStartsWith, omit}: RetrieveOpt): Promise<AxiosResponse<AddressBookResponse>> => {
        let omitString: string = "";
        for (let omitWithStart of omit) omitString += "&omit=" + omitWithStart;
        return this.request(
            this.apiAddress + `?page=${page}&zipStartsWith=${zipStartsWith}${omitString}`,
            HttpMethod.GET
        );
    }
}

const addressBookService = new AddressBookService("/address-book");
export default addressBookService;