import 'styled-components';
import {FlattenInterpolation, FlattenSimpleInterpolation, ThemedCssFunction, ThemeProps} from "styled-components";

export type BreakpointKey = "xs" | "sm" | "md" | "lg" | "xl";

declare module 'styled-components' {
    type BreakpointFunc = (key: BreakpointKey)=>string;
    type Typography = {
        fontSize: string;
        color: string;
        fontWeight: string;
    };

    export interface DefaultTheme {
        colors: {
            primary: string;
            secondary: string;
            grey: {
                main: string;
                darker: string;
                dark: string;
                generate: (strength: number)=>string;
            },
            white: string;
            black: string;
            info: string;
            error: string;
        },
        basicSpacing: number;
        spacing: (factor: number)=>string;
        pageSideMargins: number;
        pageMaxWidth: number;
        breakpoints: {
            [key in BreakpointKey]: number;
        },
        media: {
            up: BreakpointFunc;
            down: BreakpointFunc;
        },
        transition: {
            duration: string;
            generate: (properties: string[])=>string;
        },
        typography: {
            h1: Typography;
            h2: Typography;
            body: Typography;
            description: Typography;
            generate: (key: "h1" | "h2" | "body" | "description")=>FlattenInterpolation<ThemeProps<DefaultTheme>>;
        },
        borderRadius: string,
        isHover: (children: FlattenSimpleInterpolation<ThemeProps<DefaultTheme>>)
            =>FlattenInterpolation<ThemeProps<DefaultTheme>>;
    }
}