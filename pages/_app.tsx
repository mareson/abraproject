import type { AppProps } from 'next/app'
import styled, {ThemeProvider} from "styled-components";
import {THEME} from "../theme";
import GlobalStyle from "../components/GlobalStyle";
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer } from 'react-toastify';

const App = (
    {
        Component,
        pageProps
    }: AppProps
) => {
  return (
    <ThemeProvider theme={THEME}>
        <GlobalStyle />
        <Wrapper>
            <Component {...pageProps} />
        </Wrapper>
        <ToastContainer
            position="bottom-left"
        />
    </ThemeProvider>
  );
};

export default App;

const Wrapper = styled.div`
  margin-bottom: ${({theme})=>theme.spacing(3)};
`;
