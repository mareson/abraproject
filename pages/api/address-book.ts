import {NextApiRequest, NextApiResponse} from "next";
import {AddressBookItemResponse, AddressBookResponse} from "../../types/responses";
import abraService from "../../services/external/AbraService";
import basicHandler from "../../api/handler";
import {HttpMethod} from "../../services/RequestService";
import {ApiError, handleError} from "../../api/errors";
import {ZipGroupingProcessor} from "../../api/processors/ZipGroupingProcessor";
import DataGridQueryProcessor from "../../api/processors/query/DataGridQueryProcessor";
import {ZipGroupingQueryProcessor} from "../../api/processors/query/ZipGroupingQueryProcessor";

async function handleGET(
    request: NextApiRequest,
    response: NextApiResponse<AddressBookResponse>
) {
    const dataGridProcessor = new DataGridQueryProcessor(request.query);
    const zipGroupingQueryProcessor = new ZipGroupingQueryProcessor(request.query);
    const zipGroupingProcessor = new ZipGroupingProcessor(request.query);

    try {
        const groups = await zipGroupingProcessor.getGroups();

        const data = await abraService.retrieveAddressBook({
            limit: dataGridProcessor.getPageSize(),
            start: dataGridProcessor.getOffset(),
            zipStartsWith: zipGroupingQueryProcessor.getZipStartsWith(),
            omit: zipGroupingQueryProcessor.getOmit()
        });

        response.status(200).json({
            data: data.winstrom.adresar.map((item): AddressBookItemResponse => ({
                name: item.nazev,
                code: item.kod,
                zip: item.psc,
                lastUpdate: item.lastUpdate,
                id: item.id
            })),
            pagination: dataGridProcessor.getPagination(data.winstrom["@rowCount"]),
            filter: {
                zip: groups
            }
        });
    } catch (e) {
        const errorKey = e as ApiError;
        handleError(response, errorKey);
    }
}

const handler = (req: NextApiRequest, res: NextApiResponse)=>basicHandler(req, res, {
    [HttpMethod.GET]: handleGET
});
export default handler;
