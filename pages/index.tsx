import HomePage from "../components/pages/HomePage";
import addressBookService from "../services/AddressBookService";
import logger from "../logger/logger";
import {GetServerSidePropsContext, NextPage} from "next";
import {SWRConfig, unstable_serialize} from "swr";
import DataGridQueryProcessor from "../api/processors/query/DataGridQueryProcessor";
import {ZipGroupingQueryProcessor} from "../api/processors/query/ZipGroupingQueryProcessor";

interface Props {
    fallback?: Object;
}

const Home: NextPage<Props> = (
    {
        fallback
    }
) => {
    return (
        <SWRConfig value={{fallback: fallback ?? {}}}>
            <HomePage />
        </SWRConfig>
    );
};
export default Home;

/*
Uncomment if you want to prefetch the data

export async function getServerSideProps({locale, query}: GetServerSidePropsContext) {
    try {
        const dataGridQueryProcessor = new DataGridQueryProcessor(query);
        const zipGroupingQueryProcessor = new ZipGroupingQueryProcessor(query);

        const params = {
            page: dataGridQueryProcessor.getPage()+"",
            zipStartsWith: zipGroupingQueryProcessor.getZipStartsWith(),
            omit: zipGroupingQueryProcessor.getOmit()
        };

        const response = await addressBookService.retrieve(params);

        return {
            props: {
                fallback: {
                    [unstable_serialize(params)]: response.data
                }
            }
        };
    } catch (e) {
        logger.error(e);
    }

    return {props: {fallback: {}}};
}

 */