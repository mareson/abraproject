
export interface RequestType {}

export type ID = number;

export interface PaginationRequest extends RequestType {
    page?: string;
    pageSize?: string;
}

export interface AddressBookRequest extends PaginationRequest {
    zipStartsWith?: string;
    omit?: string[];
}