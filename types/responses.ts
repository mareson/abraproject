import {ID, PaginationRequest} from "./requests";

export interface ResponseType {}

export interface ErrorResponse extends ResponseType {
    message?: string;
    status: number;
}

export interface ResponseEntity extends ResponseType {
    id: ID;
    lastUpdate: Date;
}

export interface PaginationResponse extends PaginationRequest, ResponseType {
    page: string;
    pageSize: string;
    total: string;
}

export interface DataGridResponse<D> extends ResponseType {
    data: D[];
    pagination: PaginationResponse;
}


// EXTERNAL

export interface AbraAddressBookItemResponse extends ResponseEntity {
    kod: string;
    nazev: string;
    psc: string;
}

export interface AbraAddressBookResponse extends ResponseType {
    winstrom: {
        "@rowCount": string;
        adresar: AbraAddressBookItemResponse[];
    }
}

// -----

export interface AddressBookItemResponse extends ResponseEntity {
    code: string;
    name: string;
    zip: string;
}

export interface AddressBookGroupResponse extends ResponseType {
    startsWith: string;
    count: number;
}

export interface AddressBookGroupsResponse extends ResponseType {
    commonStartsWith: string;
    groups: AddressBookGroupResponse[];
    isOther: boolean;
    omit: string[];
}

export interface AddressBookResponse extends DataGridResponse<AddressBookItemResponse> {
    filter: {
        zip: AddressBookGroupsResponse;
    }
}